import { Link } from 'react-router-dom';
import imj from '../assets/imts.png'
import '../styles/home.css'
function Home() {
    
        return (
            <>
            <div className="flex items-center justify-center">
            <h1 className='txt text-4xl font-bold text-black-500 items-center justify-center txt'> Incriver vos participant </h1> 
</div>
            
            <div className='flex items-center justify-center h-screen'> 
                
                <img src={imj} alt="imj" className='max-w-full max-h-full' />
                <Link 
                to="/Form" 
                className="bg-blue-500 text-white font-bold py-2 px-4 rounded">
        Commencer
      </Link>
            </div>

           
               
            </>
        );

}

export default Home;