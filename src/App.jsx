import {Route, Routes} from "react-router-dom";
import Home from "./components/Home.jsx";
import Form from "./components/Form.jsx";


function App( ) {
  return (
    <>
        <div>
            <Routes>
                <Route path="/" element={ <Home/> } />
                <Route path="/Form" element={ <Form/> } />
            </Routes>
        </div>
    </>
  )
}

export default App
